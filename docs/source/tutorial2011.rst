---------------------------------------------
MarsWRF Data Assimilation Tutorial
---------------------------------------------

This tutorial was written in 2011 based upon an older version of the WRF GCM and DART data assimilation scheme. While the tutorial will work with the referenced versions there may be a more recent tutorial/code available.

================
Introduction
================

This tutorial outlines the Mars :abbr:`WRF (Weather Research and Forecast)`-:abbr:`DART (Data Assimilation Research Testbed)` data assimilation system. The following sections describe the installation of both MarsWRF and DART, and a guide to running an example assimilation of radiance generated from observations by the :abbr:`TES (Thermal Emission Spectrometer)`. The files discussed in this tutorial can be found in the :ref:`Files` section below. 

This document is not complete and probably contains errors and omissions caused by a) forgetting a crucially important step that I think is obvious, b) my software configuration and cluster setup, which allows me to omit certain steps (installing code) or c) errors in the tutorial. If you find an error in the code, or in this tutorial, please :ref:`tell me <tell-me>` so that I can correct it and reduce the suffering of others following this tutorial. 

The MarsWRF-DART data assimilation system was developed with funding from the `NASA Applied Information Systems Research Program <http://aisrp.nasa.gov/projects_nonav/fe78e2ff.html>`_  with early project funding and support provided by the `NASA  Mars Climate Sounder Project <http://www.nasa.gov/mission_pages/MRO/spacecraft/sc-instru-mcs.html>`_.
 

====================================
Requirements and Installation
====================================
The Data Assimilation (DA) system uses the Planetary version of the Weather Research and Forecast climate model (WRF; **SK2008**), referred to here as PlanetWRF or MarsWRF (**RT2007**). We use MarsWRF with the Data Assimilation and Research Tool (DART, **AH2009**) from :abbr:`NCAR (National Center for Atmospheric Research)`. DART controls the assimilation processes, using MarsWRF to evolve a simulation of the Martian atmosphere and an observation forward model to simulate the TES radiance observations used in the assimilation. Both the MarsWRF :abbr:`GCM General Circulation Model` and the observation forward model are controlled by DART using an interface based on on the mesoscale WRF interface used by DART. 


======================================
Weather Research and Forecast model
======================================
We use a version of PlanetWRF based on GlobalWRF 3.0.1.2. You will need to download `GlobalWRF 3.0.1.2 <http://www.mmm.ucar.edu/wrf/users/download/get_source.html>`_ from NCAR. Extract the tarfile containing WRF, then extract the **planetWRF tar file** in the same directory. The planetWRF tar file contains the functions necessary to run GlobalWRF as a Martian GCM, and contains some modifications to assist the Data Assimilation. 

Finally, you should extract the **`MarsWRF data tar file`** into a directory that you can refer to later. This directory will be the 'Data' directory for MarsWRF, containing the surface fields and radiation data. 

You will need to compile MarsWRF as a single processor model (for use within DART) and preferably as a MPI model (for spin-up and ensemble creation). You can skip the MPI compilation if you don't mind waiting a long time for the model to spin-up on a single processor. The rough guide to compiling WRF is : 

.. code-block:: bash

	./clean -a 
	./configure mars
	./compile em_global_mars
	

During the ./configure stage, you will be offered a selection of configuration options for your system, choose whichever works for you. For the MPI compile, choose the 'dmpar' option. For the single processor, chose the 'single' option. 

At this point, you should learn how to setup and run the MarsWRF model, check the `NCAR website <http://www.wrf-model.org/index.php>`_ and `PlanetWRF website <http://www.planetwrf.com>`_ for details. To create a PlanetWRF input file (wrfinput_d01) you can run :samp:`./ideal.exe` from the run directory. To run PlanetWRF in serial mode (single processor) use :samp:`./wrf.exe`. How you run WRF in parallel will depend on your cluster and type of parallelism you've chosen. 

======================================
Data Assimilation Research Testbed
======================================
We use the SVN trunk version of the DART core (everything outside of the models directory), and a custom planet_wrf model directory. You should follow the download instructions from `NCAR <http://www.image.ucar.edu/DAReS/DART/>`_ to get DART. You will need to replace one file (**cov_cutoff**) with a new file to reduce the effect of the assimilation at the model top. 

Once you have downloaded DART, you should extract the **planet_wrf directory** from planet_wrf.tar.gz and place this directory inside the models directory. This directory contains a sample radiation assimilation setup in a directory called run_rad that contains most of the data necessary to run the assimilation. You either make a clean copy of this directory before starting, or re-extract the run_rad directory from the tar file when you want to run a new assimilation. 

There are a few programs you need to compile here, but nothing too large or complex. DART uses a number of small utility programs and the main filter program. All of these programs will be compiled by running the :samp:`quickbuild.sh` script inside the run_rad directory. Before running this, you should configure the makefile template is the :file:`mkmf` directory of DART to configure the location of your NetCDF library and your compiler options. If you don't do this first, the quickbuild script will probably fail (if it fails, this is the first thing you should check!). If you have the ability to run programs in parallel, use the -mpi flag to compile a parallel filter program. 

Occasionally you will want a serial version of the filter program for debugging purposes, which can be compiled by omitting the -mpi flag (and moving the executable to filter_serial or similar to prevent it from being overwritten later). If you only want to compile the minimum number of programs you can run: 

.. code-block:: bash

	./mkmf_preprocess  ; make ; ./preprocess
	./mkmf_wrf_to_dart ; make 
	./mkmf_dart_to_wrf ; make 
	./mkmf_filter ; make ; mv filter filter_single
	./mkmf_filter -mpi ; make 
	
This will create the necessary executables, and a serial version of the filter program. After running preprocess you might get errors like: 

.. code-block:: bash
	
	file ../../../obs_def/obs_def_mod.f90 exists and will not be overwritten

or 
.. code-block:: bash
	
	file ../../../obs_kind/obs_kind_mod.f90 exists and will not be overwritten.
	
These files are automatically generated during the preprocess stage. You need to delete this files if you update the obs_def or obs_kind files or your changes won't be propagated to filter (This also means that you should never edit these files directly). Both files are deleted automatically if you compile using the quickbuild script. 


======================================
Installing the Python libraries
======================================
This tutorial uses a number of Python scripts to process the data, spin-up the ensemble, and generally hide some of the tedious steps in running the assimilation (perturbing input fields, etc.) To use these scripts, you will need a working Python 2.6 install with some (free) libraries to access and modify :abbr:`NetCDF <Network Common Data Format>` files, and to calculate times and dates on Mars. The easiest way to install these packages is to use the :samp:`easy_install` or :samp:`pip` facility available with Python. The installation process depends highly on the configuration of your workstation, and you might need to install additional compiled libraries to make the programs work. Unfortunately this is impossible to test on my configuration for other configurations. 

You will need at least the `pyhdf <http://pypi.python.org/pypi/pyhdf/0.8.3>`_, `numpy <http://pypi.python.org/pypi/numpy/1.5.1>`_, and the minimal **Ashima libraries** included in the python code directory. You could probably :samp:`easy_install` the Ashima Python library from the scripts directory using:  

.. code-block:: bash

	easy_install Ashima-0.19A-py2.6.egg
	
and it will hopefully find all of the required libraries for you. 


===================
Model Spinup
===================
You will need to spin-up the MarsWRF GCM to create the ensemble: Copy the 'run' directory from the MPI WRF compile directory to a directory (called e.g. spin-up). copy the :file:`namelist.spinup` from the tutorial directory to this spin-up directory and rename to namelist.input. The namelist.spinup contains the configuration options to integrate the model from rest for 1000 sols (~1.8 Martian years) to ensure that it reaches a steady state. You should also copy the ideal.exe file from the serial WRF directory to initialize the model correctly. 
 
To start the spin-up, run ./ideal.exe to create a wrfinput_d01 file, then run WRF. WRF will take about 360 CPU hours per Martian year in the configuration used here. At the end of the spin-up, you should have some wrfout* and wrfrst* files. The last of these files (with 44588 in the filename) will be used to create the ensemble. 


=====================
Perturbing the model
=====================
 
Once the model has 'spun-up' you will need to create the ensemble members from a model state approximately 30 days prior to the start of the assimilation. The sample observation file contains data starting from 5am Mars day (MSD) 44,587. The spin-up integration will have produced an output 30 days prior to the start of the assimilation, which will be used to generate the ensemble that will integrate for the remaining 30 days before the start of the assimilation, outputting a file at 12pm, MSD 44,587. Note that WRF and DART currently have different interpretations of which day is the first day of the year. DART thinks 0, WRF thinks 1, hence the WRF files that correspond to day 44,587 are labeled 44588. 

To perturb the model, use the python script called :file:`perturb_wrfinput.py`. This program takes the filename as an argument and a series of comma separated fields to determine which fields to perturb and how to perturb them. The argument ``--gaussian`` perturbs fields with an additive random (Gaussian) noise. For each field you want to perturb you will need the Field name, the Gaussian mean value (default is 0), the Gaussian standard deviation (1 sigma, default is 0), and optional low and high bounds on the final value. 

In the example assimilation, we want to include temperature, wind, surface temperature, albedo, emissivity, and column dust opacity in the state vector, so these fields should each be perturbed. A suitable command would look something like  

.. code-block:: bash

	./python_code/perturb_wrfinput.py filename --gaussian temp,0,5 \
                                       uwind,0,5 \
                                       vwind,0,5 \
                                       tsk,0,5 \
                                       tau_od2d,0.0,0.03,0,0.5 \
                                       albedo,0,0.1,0,1 \
                                       emissivity,0,0.1,0,1


In this example *temp* is special and corresponds to a kinetic temperature perturbation, not a potential temperature perturbation (perturb T for that), *uwind* and *vwind* deal with occurrences of the fields "U" or "U_1" and "U_2" (and the same with "V"), *albedo* and *emissivity* perturbs the instantaneous albedo/emissivity (*albedo* and *emiss*) and the background *albbck* and *embck* fields. Note that this process is destructive as the input file is modified, so make a copy of the file before running this command. (Don't run the command until you've read the next section).  

======================================
Preparing the ensemble
======================================
To create the ensemble, you need to perturb (say) 20 ensemble members, and integrate each ensemble member to the start of the assimilation at 12pm, MSD 44,587. There is a script called :samp:`spinup_ensemble` that will setup the ensemble. The script expects a directory called :file:`ens_template` to exist, which should contain (copies of, or links to) :samp:`wrf.exe`, :samp:`ideal.exe`, the :file:`Data` directory, a suitable :file:`namelist.input` file, and the :file:`wrfrst_d01_0001-44558_12:00:00` file created by the initial spinup. The namelist.input should be one that will integrate WRF for 30 days from day 44558, outputting at least on the last time-step. The file :file:`namelist.perturbed` in the tutorial directory is an example of this namelist file that should work for this case (it needs to be renamed to namelist.input to be identified by WRF). 

To run the spinup_ensemble program, first make a directory to hold the ensemble members. Then create and populate the *ens_template* directory with the files listed above, then run the :samp:`spinup_ensemble` command. This will generate 20 directories (e01 through e20) and perturb the input file in each directory. It won't run each ensemble member; that's up to you to do depending on your cluster/ workstation setup. Each 30 day integration will take about 1 CPU hour. 

Once all of the ensemble members have been integrated to day 44,588, you will need to copy each of the *wrfrst_d01_0001-44588_12:00:00* files into the same directory, appending a four digit counter to each, starting at 0001. If you used the :samp:`spinup_ensemble` script to create the ensemble directories, then the :samp:`create_ens_data` script will create a directory called *ens_data*, and will copy the output from each ensemble member into that directory with the correct names. 


======================================
TES radiance observations
======================================
We will be assimilating observations from the Thermal Emission Spectrometer (TES; **CB2001**) instrument that flew on the :abbr:`MGS (Mars Global Surveyor)` satellite between 1999 and 2006. TES observed the atmosphere of Mars between 200 and 1700 cm :sup:`-1` with a resolution of 5.29 cm :sup:`-1` (in its *high resolution* mode). We will use observations from 572cm :sup:`-1` to 815cm :sup:`-1` in our assimilation. 

To prepare the TES data for use in the data assimilation we need to extract the data from the TES database, using the `vanilla <http://tes.asu.edu/documentation/index.html>`_ tool, then convert this data to the format that DART expects. 

The first step, extracting the data using the vanilla tool, depends somewhat on the criteria you wish to place on the input data. Specific details on the usage of vanilla should be checked on the TES website, where you can also find links to the TES database. The vanilla command we used to generate the tutorial input data is:  

.. code-block:: bash

	./vanilla -fields 'ephemeris_time longitude latitude             \
	solar_longitude local_time detector_number emission_angle        \
	scan_length detector_mask spectral_mask target_temperature       \
	surface_pressure rad.calibrated_radiance[41:64] obs.scan_length  \
	detector_number ' -select 'quality:major_phase_inversion 0 0     \
	quality:spectrometer_noise 0 1 quality:detector_mask_problem 0 0 \
	quality:calibration_quality 0 0 quality:calibration_failure 0 0  \
	quality:temperature_profile_rating 0 0  target_temp 100 300      \
	emission_angle 0 10  ephemeris_time -18144000 -17000000          \
	obs.scan_length 1 1 detector_number 2 2' ~/tes_data/data         \
	> output_temp_ephem_-18144000_-17000000_rad

This command will extract all observation between -18,144,000 and -1,700,000 seconds after January 1, 2000, selecting for emission angle, target temperature, detector number, and scan resolution (length). We choose radiance bands 41:64 as they contain the region between 572cm :sup:`-1` and 815cm :sup:`-1`.

Once this file is generated, you then need to convert this to a DART input file. This vanilla command generates approximately 1.3 million observations, each with 24 radiance bands. To use the data within DART we want to grid the data into 5x5 degree horizontal boxes (using observations within about 1 minute of each other). Once the data is gridded we then output each of the gridded 'super' observations as an individual observation, separated by wavenumber, creating 1.6 million observations. For each observation, we use the ephemeris time and location from the TES database to calculate the time and location on Mars of the observation, and use the observation meta-data (scan length, emission angle, etc.) to describe the observation to the forward model. 

This process is performed by the script :samp:`process_tes_gridded_radiance.py` which takes as input the name of the file containing the vanilla output, the maximum number of observations to grid into one bin, and the maximum total number of observations allowed. The number of observations to grid is typically set to 1,000. In practice it is very rare to get more than 40 observations in one grid box because of the MGS orbit. This threshold is meaningful only below about 40 because of this (1 would imply no gridding). 

To process the vanilla generated file, run 

.. code-block:: bash

	process_tes_gridded_radiance.py output_temp_ephem_-18144000_-17000000_rad 1000 1000000
	
At the end of this program, a file called :file:`obs_seq.out` will be generated. This file is the DART input file. If you don't want to process the data yourself, the :file:`tutorial input` file is an example DART input file that contains observations from MSD 44,587 to MSD 44,593. This file can be used in the tutorial assimilation. 

======================================
Preparing the run directory
======================================
At this point, you should have the output from the ensemble members at the correct date and time for the observations, the updated DART and planet_wrf directories, and the observation input file. To run the assimilation you will need to setup a directory (e.g. the run_rad directory) to use as the assimilation directory. 

In this directory, you will need (a copy of or link to) the wrf.exe executable, the MarsWRF Data directory, and a namelist.input file. The namelist.input file that exists in the run_rad directory will work if the Data directory exists (or is linked ) in the local directory (./). You will also need (a copy of or link to) the ens_data directory where you stored the ensemble output. 

To prepare the ensemble for the assimilation, copy the executable :samp:`wrf_to_dart` and the :file:`input.nml` file into the ens_data directory. You will need to run the :samp:`wrf_to_dart` executable on each of the ensemble outputs after renaming it to wrfinput_d01. The script :samp:`process_wrfout_files` from the tutorial directory is designed to do this automatically for you. Copy the :file:`input.nml` and :file:`wrf_to_dart` files from :file:`run_rad` into :file:`ens_data`, and copy the ``process_wrfout_files`` script into :file:`ens_data`, and run:

.. code-block:: bash
	
	./process_wrfout_files wrfout_d01_0001-44588_12:00:00
	
The filename you give to the script is the filename of the wrfout files without the counter you added earlier. This process will generate files called :file:`filter_ics.X`, where X corresponds to the counter number of the wrfout files. Copy each of the filter_ics files and the wrfout files to the run_rad directory, and copy (any) one of the wrfout files to a file called :file:`wrfinput_d01` in the run_rad directory. The wrfinput_d01 file is used as the source of fixed variables from WRF, such as the horizontal grid-box locations. 

Finally, copy the observation file into the run_rad directory. This file should be called (or linked to) :file:`obs_seq.out` so that DART can read it. This should be sufficient to run the filter program. 

======================================
Running the assimilation
======================================
 
The assimilation is run using the filter program. If the parallel version is used, it needs to be run under MPI (how you do that depends on your cluster). If the serial version is used, it can be run on the command line but will be slower. The supplied :file:`input.nml` file is configured to run for 1 day, assimilating every hour. This assimilation will take between 1 hour and 20 hours, depending on the number of processors you use (1-20). The result of the assimilation is a large number of log files (wrf.out.*) and wrfout files (wrfout_*:00) as well as a :file:`Posterior_Diag.nc`, a :file:`Prior_Diag.nc`, and an :file:`obs_seq.final` metadata file.


To run the longer assimilation. Reset the directory to the state before the run (delete the wrfout files, wrf.out files, *Diag.nc, assim_state*. Copy the original wrfout and filter_ics files back in, etc.). Change the "last_obs_days" entry in the filter_nml namelist (in input.nml) to be 44594, and re-run filter. The tutorial input data runs to MSD 44,594. 

===================
Output Files
===================
At the end of the assimilation, you will have a number of new files. The log files (wrf.out*) contain the wrf output for every ensemble member for every assimilation step that is run (typically 20 files every hour). These files are only useful if the assimilation fails inside one of the WRF models. The wrfout* files are the output files from the last ensemble member to finish each assimilation step, and are not very useful on their own. 

The :file:`Prior_Diag.nc` and :file:`Posterior_Diag.nc` files contain the state of every ensemble member before (Prior) and after (Posterior) each assimilation step. Essentially this means hourly output from WRF for 20 members, but occasionally there are missing outputs if there were no data to assimilate during a particular period. These files can get big very quickly, generating about 2.5GB of data for every simulated day. 

The :file:`obs_seq.final` file contains every observation from the :file:`obs_seq.out` file, with observation errors, the prior and posterior ensemble prediction and the ensemble spread of predictions. You can use this file to diagnose the skill of the assimilation/ model by comparing each observation with it's predictions.

You can also convert this data to a more digestible NetCDF file using the :samp:`obs_seq_to_netcdf` executable you compiled with :samp:`quickbuild.sh`. This program reads in the :file:`input.nml` namelist file and the :file:`obs_seq.final` file and stores all of the observations in a NetCDF file that can be read in any compatible program. The program :samp:`obs_diag` in the same directory can be used to aggregate the data in the :file:`obs_seq.final` file. As currently configured, the :samp:`obs_diag` program will aggregate observations into hourly bins and radiance bins, regardless of location, and calculate some useful diagnostics of the assimilation based on this gridding. The output file (obs_diag.nc) can be used with the Matlab scripts supplied by NCAR (in the DART/matlab directory) to plot some basic diagnostics. 

An example of the output from this model is included below. This output file is a processed version of the Prior file to include only the mean and variance information for 20 sols of an assimilation in a `CF Metadata convention <http://cf-pcmdi.llnl.gov/>`_ conformant file. This file is intended as an example of the output available from the assimilation, but should not be considered the final reanalysis product. 

.. _Files:

=======
Files
=======
The files listed below are all contained in the tar file `tutorial_2011_08_19.tar <code/tutorial_2011_08_19.tar>`_. Untar this file to create the :file:`tutorial` directory that contains all of the files described in the text.

* The **tarfiles/planetWRF3.0.1.2DA.tar.gz** file contains updated functions that will convert GlobalWRF to MarsWRF with the new a radiation scheme and carbon dioxide microphysics scheme, among other changes. It is compatible with GlobalWRF 3.0.1.2 only.
* The **tarfiles/Data.tar.gz** file contains parameters and data used by the radiation scheme as well as surface data used to initialize the GCM, such as surface albedo, emissivity, thermal inertia and topography.
* The **fortran/cov_cutoff_mod.f90** file contains a modified cov_cutoff function for DART. It needs to be copied to the DART/cov_cutoff directory to be included in the compile.
* The models/planet_wrf directory, contained in the **tarfiles/planet_wrf_dart.tar.gz** file, contains the planetWRF interface for DART, including a model interface to read the model state vector from within DART, the forward model to simulate TES radiance observations, and some configuration and run scripts.
* The Ashima Python (in **Ashima-0.19A-py2.6.egg**) library contains a few essential classes to work with the Python scripts used by the tutorial. For example, the Mars24 class implements the Mars24 timing scheme (**AM2000**) used inside the GCM and DART to place observations accurately and precisely.
* The **namelists/namelist.spinup** file is configured to integrate the model for 1,000 days and end the simulation by outputting a wrfrst and wrfout file 30 days before the start of the tutorial assimilation.
* The **scripts/perturb_wrfinput.py** script will take a wrf output file and perturb variables according to the inputs given on the command line. The script is destructive in that it modifies the file in place and does not make a copy first.
* The **scripts/spinup_ensemble** script is used to perturb the ensemble members and prepare WRF to run for 30 days with the perturbed initial state.
* The **namelists/namelist.perturbed** file is configured to work with the spinup_ensemble script to run WRF for 30 days upto the start of the assimilation.
* The **scripts/create_ens_data** program will collect all of the wrfout files generated by the ensemble created by :samp:`spinup_ensemble`, storing them in a directory called ens_data.
* The **scripts/process_tes_gridded_radiance.py** script processes output from a vanilla run to create the DART input file.
* The **tarfiles/tutorial_input.gz** contains a sample DART input file with about 5 sols of data
* The **scripts/process_wrfout** script runs the wrf_to_dart program on each of the ensemble members.

The `Ls150_160.nc <code/Ls150_160.nc>`_ NetCDF file is a CF 1.0 conformant file that gives an example of the reanalysis output from this data assimilation. This example contains 20 sols of daily data for atmospheric temperature, horizontal wind, and the surface properties used in the assimilation.

===================
References
===================

* **AM2000** M. Allison and M. McEwan. A post-Pathfinder evaluation of aerocentric solar coordinates with improved timing recipes for Mars seasonal/diurnal climate studies. Planet. Space Sci., 48:215–235, 2000.
* **AH2009** J. L. Anderson, T. Hoar, K. Raeder, H. Liu, N. Collins, R. Torn, and A. Arellano. The data assimilation research testbed: A community facility. Bul. Amer. Met. Soc., 90:1283–1296, 2009.
* **CB2001** P. R. Christensen, J. L. Bandfield, V. E. Hamilton, S. W. Ruff, H. H. Kieffer, T. N. Titus, M. C. Malin, R. V. Morris, M. D. Lane, R. L. Clark, B. M. Jakosky, M. T. Mellon, J. C. Pearl, B. J. Conrath, M. D. Smith, R. T. Clancy, R. O. Kuzmin, T. Roush, G. L. Mehall, N. Gorelick, K. Bender, K. Murray, S. Dason, E. Greene, S. Silverman, and M. Greenfield. Mars Global Surveyor Thermal Emission Spectrometer experiment: Investigation description and surface science results. J. Geophys. Res., E10:106:23823–23872, 2001.
* **RT2007** M. I. Richardson, A. D. Toigo, and C. E. Newman. PlanetWRF: a general purpose, local to global numerical model for planetary atmospheric and climate dynamics. J. Geophys. Res., E112:E09001, 2007.
* **SK2008** W. C. Skamarock and J. B. Klemp. A time-split non-hydrostatic atmospheric model for weather research and forecasting applications. j. Comp. Phys., 227:7:3465–3485, 2008.


.. _tell-me:

===================
Contact
===================
Christopher Lee, `Ashima Research <http://www.ashimaresearch.com>`_. :email:`lee@ashimaresearch.com`.



